FROM golang:1.18

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download


COPY *.go ./

RUN go build -o /simple-syslog-streaming-sidecar

CMD ["/simple-syslog-streaming-sidecar"]
