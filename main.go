package main

import (
	//standard
	"errors"
	"fmt"
	"log"
	"log/syslog"
	"os"
	"time"

	// external
	"github.com/hpcloud/tail"
	"github.com/spf13/viper"
)

type SyslogSidercarConfig struct {
	SyslogServer      string
	ClientAppName     string
	ClientLogFile     string
	FileCreateTimeout int
	FileIdleTimeout   int
}

func loadConfigs() (SyslogSidercarConfig, error) {
	newConfig := SyslogSidercarConfig{}

	// Set config params
	viper.SetConfigType("env")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		log.Println("Error reading .env file for settings.")
		_, newErr := os.Stat(".env")
		if newErr == nil {
			return newConfig, err
		}
		log.Println(".env file does not exist, continuing.")
	}
	newConfig.SyslogServer = viper.GetString(SYSLOG_SERVER)
	if newConfig.SyslogServer == "" {
		return newConfig, errors.New("SYSLOG_SERVER not set")
	}

	newConfig.ClientAppName = viper.GetString(CLIENT_APP_NAME)
	if newConfig.ClientAppName == "" {
		return newConfig, errors.New("CLIENT_APP_NAME not set")
	}

	newConfig.ClientLogFile = viper.GetString(CLIENT_LOG_FILE)
	if newConfig.ClientLogFile == "" {
		return newConfig, errors.New("CLIENT_LOG_FILE not set")
	}

	viper.SetDefault(FILE_CREATE_TIMEOUT, DEFAULT_TIMEOUT)
	newConfig.FileCreateTimeout = viper.GetInt(FILE_CREATE_TIMEOUT)

	viper.SetDefault(FILE_IDLE_TIMEOUT, DEFAULT_TIMEOUT)
	newConfig.FileIdleTimeout = viper.GetInt(FILE_IDLE_TIMEOUT)

	return newConfig, nil
}

func waitForFile(cfg SyslogSidercarConfig) error {
	for i := 0; i < cfg.FileCreateTimeout; i++ {
		info, err := os.Stat(cfg.ClientLogFile)
		if err == nil {
			if info.IsDir() {
				return errors.New("File is directory :(")
			}

			return nil
		}
		time.Sleep(1 * time.Second)
	}

	return errors.New(fmt.Sprintf("Log file %s does not exist after %d seconds, exiting", cfg.ClientLogFile, cfg.FileCreateTimeout))
}

func main() {

	cfg, err := loadConfigs()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Simple Syslog Streaming Sidecar Starting")

	sysLog, err := syslog.Dial("tcp", cfg.SyslogServer, syslog.LOG_INFO, "simple-syslog-steaming-sidecar")
	if err != nil {
		log.Fatal(err)
	}
	defer sysLog.Close()

	if err := waitForFile(cfg); err != nil {
		fmt.Fprint(sysLog, err)
		log.Fatal(err)
	}

	fmt.Fprintf(sysLog, "Client log file %s is ready for reading", cfg.ClientLogFile)

	sysLog_app, err := syslog.Dial("tcp", cfg.SyslogServer, syslog.LOG_INFO, cfg.ClientAppName)
	if err != nil {
		fmt.Fprint(sysLog, err)
		log.Fatal(err)
	}
	defer sysLog_app.Close()

	logLine, err := tail.TailFile(cfg.ClientLogFile, tail.Config{Follow: true, ReOpen: true})
	if err != nil {
		fmt.Fprintf(sysLog, "unable to read file %s: %s\n", cfg.ClientLogFile, err.Error())
		log.Fatal(err)
	}

logRead:
	for {
		select {
		case line := <-logLine.Lines:
			fmt.Fprint(sysLog_app, line.Text)
		case <-time.After(time.Duration(cfg.FileIdleTimeout) * time.Second):
			fmt.Fprintf(sysLog, "No additional log lines written to %s, closing.", cfg.ClientLogFile)
			break logRead

		}
	}

}
