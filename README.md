Simple Syslog Streaming Sidecar
---

K8s sidecar for streaming logfiles to syslog. Reads target file until nothing has been written for the confgiured timeout.

### Environment Variables
SYSLOG\_SERVER - what syslog server to write to, required eg: syslog.example.com:514
CLIENT\_APP\_NAME - what application is actually writing the log file, required eg: sample-application
CLIENT\_LOG\_FILE - where is the file located, required eg: /var/log/sample-application.log
FILE\_CREATE\_TIMEOUT - how long to wait for the file to be created, default 300 seconds
FILE\_IDLE\_TIMEOUT - how long to wait for the file to be update before closing, default 300 seconds

[Sample Kubernetes Deployment](sample-deployment.yml)

